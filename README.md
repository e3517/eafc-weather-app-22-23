# EAFC WEATHER APP 22-23

## Objectifs

Créer une application permettant de consulter la météo de l'endroit où on se trouve (après avoir activé la géolocalisation JS) à partir de l'API [OpenWeatherAPI](https://openweathermap.org/api)

## User stories

- [ ] L'internaute peut voir la météo instantannée de l'endroit où il se trouve. (Température, état du ciel, date et heure exacte, nom de la localité et image de fond correspondant à la météo et à l'heure)
- [ ] L'internaute peut voir les prévisions (températures et statut du ciel) pour les 10 prochaines heures
- [ ] L'internaute peut voir les prévisions (températures min et max et statut du ciel) pour les 8 prochains jours

## Contraintes techniques

- [ ] Application en VUE JS composition API avec le bundler 'Vite'
- [ ] Décomposition en components
- [ ] Utilisation d'un store avec 'Pinia'
- [ ] Requêtes AJAX avec 'Axios'

## Organisation du travail

- [ ] Le travail est organisé en SPRINTS
- [ ] Chaque SPRINT reprend un ensemble de spécification techniques
- [ ] Les développeurs choisissent la ou les spécification techniques qu'ils pourront assumer sur la semaine
- [ ] Chaque SPRINT est évalué (validé ou non) au bout d'une semaine

## Gestion du dépôt commun

- [ ] La première étape est de cloner ce dépôt sur votre machine locale
- [ ] Avant de vous attribuer une tâchen vous Pullez la branche develop
- [ ] Vous ne travaillez JAMAIS sur la branch 'main' ou 'develop'
- [ ] Une fois que vous vous êtes atribué une spécification technique acceptée (vous vous choisissez dans la partie 'Assignee'), Vous lancez un 'Create merge Request and Branch' et vous passez cette spécification en mode 'Doing'
- [ ] Vous passez sur la branche qui correspond à votre Feature
- [ ] vous développez votre Feature et vous faites des commits lors des étapes importantes de votre développement
- [ ] une fois le développement de votre Feature terminé, vous faites un commit final
- [ ] Vous attribuez un Reviewer à votre Merge Request et vous passez votre Feature en mode 'Awaiting Feedback'
- [ ] Chaque demande de review doit être traitée sérieusement car vous partagez la responsabilité du code produit ou validé. Vous pouvez valider la Feature ou faire un commentaire de modification
- [ ] Si votre Feature est validée, vous la passez en mode 'To be Merged' sinon, vous la repassez en mode 'Doing'
- [ ] Je ferai moi-même les merges de chaque SPRINT et passerai les Features en mode 'closed' si le SPRINT est validé.

## Test and Deploy

L'application sera déployée sur les pages Gitlab
