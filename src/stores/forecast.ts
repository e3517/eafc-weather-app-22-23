import { ref, computed, reactive } from "vue";
import { defineStore } from "pinia";
import { useCoordsStore } from "./coords";
import axios from "axios";

export const useForecastStore = defineStore("forecast", () => {
    const forecast = ref<Object>({});
    const current = ref<Object>({});
    const daily = ref<Object>({});
    const hourly = ref<Object>({});
    const loader = ref<boolean>(true);

    const fetchForecast = async () => {
        const coordsStore = useCoordsStore();
        const coords:any = await coordsStore.coords();

        try{
            const data = await axios.get(
                `https://api.openweathermap.org/data/2.5/onecall?lat=${coords.latitude}&lon=${coords.longitude}&exclude=minutely&units=metric&appid=b68afb69c2607c15cb4f6bf022f17e25`
            );
            forecast.value = data.data;
            current.value = data.data.current;
            daily.value = data.data.daily;
            hourly.value = data.data.hourly;
            loader.value = false;
        }catch(error){
            return error;
        }
    }

    return { forecast, current, daily, hourly, loader, fetchForecast}
});